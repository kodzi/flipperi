#include <QDebug>
#include <QPainter>
#include "blockeritem.h"


BlockerItem::BlockerItem(QGraphicsItem *parent): QGraphicsPixmapItem(parent)
{
}

BlockerItem::BlockerItem(const QString &fileName, const QPixmap &pixmap, const QPointF &pos, QGraphicsItem *parent)
: QGraphicsPixmapItem(pixmap, parent),
  m_bounce(0.8), m_score(100), m_pos(pos)
{
    setFlag(QGraphicsItem::ItemIsSelectable);
    int index1;
    //If filename is in format: folder/filename.extension cut it so it is only file name
    index1 =  fileName.lastIndexOf("/");
    m_fileName = fileName.mid(index1+1);

    index1 = m_fileName.indexOf("_");
    // if fileName is in the following format: name_double_int.png
    if(index1 != -1){
        index1 += 1;
        int index2  = m_fileName.indexOf("_", index1) + 1;
        int index3 = m_fileName.indexOf(".", index2);
        m_bounce = m_fileName.mid(index1, index2-index1-1).toDouble() / 100;
        m_score = m_fileName.mid(index2, index3 - index2).toInt();
    }
    QGraphicsPixmapItem::setPos(m_pos);
}



QString BlockerItem::getFileName() const
{
    return m_fileName;
}

QPointF BlockerItem::getPos() const
{
    return pos();
}
