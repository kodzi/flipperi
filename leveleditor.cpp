#include "leveleditor.h"
#include <QDebug>
#include <QFileDialog>
#include <QLineEdit>
#include <QStandardItemModel>
#include <QtGui>
#include <QLabel>
#include "mainwindow.h"

LevelEditor::LevelEditor(Database *database, PlayWidget *playWidget, QWidget *parent) :
    QDialog(parent),
    m_playWidget(playWidget)
{
    setMinimumHeight(600);
    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    this->setLayout(mainLayout);
    mainLayout->addWidget(new QLabel("Select Level:"));
    m_levelListView = new QListView(this);
    mainLayout->addWidget(m_levelListView);


    QHBoxLayout* buttonLayout = new QHBoxLayout(this);
    m_newButton = new QToolButton(this);
    m_newButton->setMinimumSize(48,24);
    m_newButton->setStyleSheet("QToolButton{border-image: url(://ui/t_add.png)} QToolButton:hover{border: 2px solid #8f8f91; image: url(://ui/t_add_hovered.png)}");
    buttonLayout->addWidget(m_newButton);
    m_editButton = new QToolButton(this);
    m_editButton->setMinimumSize(48,24);
    m_editButton->setText("&Edit");
    buttonLayout->addWidget(m_editButton);
    m_removeButton = new QToolButton(this);
    m_removeButton->setMinimumSize(48,24);
    m_removeButton->setStyleSheet("QToolButton{border-image: url(://ui/t_remove.png)} QToolButton:hover{border: 2px solid #8f8f91; image: url(://ui/t_remove_hovered.png)}");
    buttonLayout->addWidget(m_removeButton);
    mainLayout->addLayout(buttonLayout);


    m_ratebar = new RateBar(this);
    m_ratebar->setMinimumHeight(31);
    m_ratebar->setMinimumWidth(131);
    mainLayout->addWidget(m_ratebar, 0, Qt::AlignHCenter);


    QHBoxLayout* imageLayout = new QHBoxLayout(this);
    m_backgroundButton = new QToolButton(this);
    m_backgroundButton->setText("&Background");
    m_maskButton = new QToolButton(this);
    m_maskButton->setText("&Mask");
    imageLayout->addWidget(m_backgroundButton);
    imageLayout->addWidget(m_maskButton);
    mainLayout->addLayout(imageLayout);

    mainLayout->addWidget(new QLabel("Select Blocker:"));
    m_blockerListView = new QListView(this);
    mainLayout->addWidget(m_blockerListView);


    m_playButton = new QPushButton("&Play");
    mainLayout->addWidget(m_playButton);
    m_playButton->setVisible(false);

    m_backButton = new QPushButton("&Back");
    mainLayout->addWidget(m_backButton);



    connect(m_newButton, SIGNAL(clicked()), this, SLOT(onNewButtonClicked()));
    connect(m_removeButton, SIGNAL(clicked()), this, SLOT(onRemoveButtonClicked()));
    connect(m_editButton, SIGNAL(clicked()), this, SLOT(onEditButtonClicked()));
    connect(m_backgroundButton, SIGNAL(clicked()), this, SLOT(onBackgroundButtonClicked()));
    connect(m_maskButton, SIGNAL(clicked()), this, SLOT(onMaskButtonClicked()));
    connect(m_backButton, SIGNAL(clicked()), this, SLOT(onBackButtonClicked()));
    connect(m_playButton, SIGNAL(clicked()), this, SLOT(onPlayButtonClicked()));

    m_database = database;
    m_editMode = false;
    setEditMode(false);

    m_levelListView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_levelListView->setModel(m_database->getModel());
    m_levelListView->setSelectionMode(QAbstractItemView::SingleSelection);


    m_blockerListView->setViewMode(QListView::IconMode);
    m_blockerListView->setDragDropMode(QAbstractItemView::DragOnly);
    m_blockerListView->setAcceptDrops(true);
    m_blockerListView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_blockerListView->setModel(m_database->getBlockModel());
    m_blockerListView->setVisible(false);

    //Add stylesheet to all toolbuttons so that they have hover capabilites
    m_newButton->setStyleSheet("QToolButton{border-image: url(://ui/t_add.png)} QToolButton:hover{border: 2px solid #8f8f91; image: url(://ui/t_add_hovered.png)}");
    m_removeButton->setStyleSheet("QToolButton{border-image: url(://ui/t_remove.png)} QToolButton:hover{border: 2px solid #8f8f91; image: url(://ui/t_remove_hovered.png)}");

    m_ratebar->setVisible(false);
    connect(m_ratebar, SIGNAL(ratingChanged(int)), this, SLOT(on_ratingChanged(int)));

    //Revert to original
    connect(this, SIGNAL(rejected()), this, SLOT(onSaveRejected()));

}

LevelEditor::~LevelEditor()
{
    onSaveRejected();
}

void LevelEditor::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}

void LevelEditor::dragMoveEvent(QDragMoveEvent *event)
{
    event->accept();
}


void LevelEditor::keyPressEvent(QKeyEvent *key)
{
    if(key->key() == Qt::Key_Delete &&
            m_editMode){
        m_playWidget->deleteSelected();
    }
}




//Create a dialog where user can input name for new level
//Switch to edit mode when accepts signal is received
void LevelEditor::onNewButtonClicked()
{
    QDialog nameDialog;
    nameDialog.setWindowTitle("Insert level name!");
    QVBoxLayout verticalLayout;
    QLabel label("Name:");
    verticalLayout.addWidget(&label);
    QLineEdit lineEdit;
    lineEdit.setPlaceholderText("ie. myNewLevel");
    verticalLayout.addWidget(&lineEdit);
    QHBoxLayout buttonLayout;
    QPushButton okButton("&OK");
    QPushButton cancelButton("&Cancel");
    connect(&okButton, SIGNAL(clicked()), &nameDialog, SLOT(accept()));
    connect(&cancelButton, SIGNAL(clicked()), &nameDialog, SLOT(reject()));

    buttonLayout.addWidget(&okButton);
    buttonLayout.addWidget(&cancelButton);
    verticalLayout.addLayout(&buttonLayout);
    nameDialog.setLayout(&verticalLayout);

    if(nameDialog.exec() == QDialog::Accepted){
        //Name exists
        QList<QStandardItem*> list = m_database->getModel()->findItems(lineEdit.text());
        if(list.size() != 0){
            m_levelListView->setCurrentIndex(m_database->getModel()->indexFromItem(list.at(0)));
            return;
        }
        m_levelListView->setCurrentIndex(m_database->addLevelIndex(lineEdit.text()));
        setEditMode(m_editMode = true);
        m_selectedIndex = m_levelListView->currentIndex();
        m_playWidget->setupLevel(lineEdit.text(), 0, 0, lineEdit.text() + DEFAULT_FILETYPE);
    }
}

void LevelEditor::onSaveRejected()
{

    setEditMode(m_editMode = false);
    emit changePage(BACK);

}

void LevelEditor::onRemoveButtonClicked()
{
    QModelIndex tmpIndex = m_levelListView->currentIndex();
    m_database->removeLevel(tmpIndex);
}


void LevelEditor::onBackButtonClicked()
{
    //discard all changes made to level
    m_playWidget->saveChangesToLevel(false);
    close();
}

void LevelEditor::onEditButtonClicked()
{
    //If there is no levels in model then do nothing
    if(m_database->getModel()->rowCount() == 0){
        return;
    }

    m_selectedIndex = m_levelListView->currentIndex();
    setEditMode(m_editMode = !m_editMode);

    //Create new Level-object and add exsisting information from model to it
    if(m_editMode){
        m_selectedIndex = m_levelListView->currentIndex();
        int row = m_selectedIndex.row();
        QString name =  m_database->getModel()->item(row, 0)->text();
        int playCount =  m_database->getModel()->item(row, 1)->text().toInt();
        int rating =  0;
        QString filePath = m_database->getModel()->item(row, 3)->text();
        //Here start editing
        m_playWidget->startEdit();
        m_playWidget->setupLevel(name,QString(playCount),QString(rating),filePath);

    }
    else{
        //Here stop editing
        m_playWidget->saveChangesToLevel(true);
    }
}

void LevelEditor::setEditMode(bool mode)
{
    if(mode){
        //Lock current selection
        m_levelListView->setSelectionMode(QAbstractItemView::NoSelection);
        m_editButton->setText("&Save");
        m_newButton->setDisabled(true);
        m_removeButton->setDisabled(true);
        m_maskButton->setDisabled(false);
        m_maskButton->setVisible(true);
        m_backgroundButton->setDisabled(false);
        m_backgroundButton->setVisible(true);
        m_selectedIndex = m_levelListView->currentIndex();
        QStandardItem* item = m_database->getModel()->item(m_selectedIndex.row(),2);
        int rating = item->text().toInt();
        m_ratebar->setRating(rating);
        m_ratebar->setVisible(true);
        m_playButton->setVisible(false);
        //Drag & Drog
        grabKeyboard();
        m_blockerListView->setVisible(true);
        connect(m_blockerListView, SIGNAL(pressed(QModelIndex)) , this, SLOT(makeDrag(QModelIndex)));
    }
    else{
        m_levelListView->setSelectionMode(QAbstractItemView::SingleSelection);
        m_editButton->setText("&Edit");
        m_newButton->setDisabled(false);
        m_removeButton->setDisabled(false);
        m_maskButton->setDisabled(true);
        m_maskButton->setVisible(false);
        m_backgroundButton->setDisabled(true);
        m_backgroundButton->setVisible(false);
        m_ratebar->setVisible(false);
        m_blockerListView->setVisible(false);
        m_playButton->setVisible(true);
        //Drag & Drog
        releaseKeyboard();
        disconnect(m_blockerListView, SIGNAL(pressed(QModelIndex)) , this, SLOT(makeDrag(QModelIndex)));
    }
}

void LevelEditor::onBackgroundButtonClicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select Background Image","./tausta/", "Image Files (*.png *.jpg *.bmp)");
    QString url = DEFAULT_BACKGROUND_DIR + fileName.mid(fileName.lastIndexOf("/")+1);
    m_playWidget->setBackgroundImage(url);
}

void LevelEditor::onMaskButtonClicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select Mask Image","./pelialue/", "Image Files (*.png *.jpg *.bmp)");
    QString url = DEFAULT_MASK_DIR + fileName.mid(fileName.lastIndexOf("/")+1);
    m_playWidget->setMaskImage(url);
}

void LevelEditor::onPlayButtonClicked()
{
    //discard all changes made to level
    m_playWidget->saveChangesToLevel(false);
    close();

    int row = m_levelListView->currentIndex().row();
    QString name = m_database->getModel()->item(row,0)->text();

    QString playCount = m_database->getModel()->item(row,1)->text();
    QString rating = m_database->getModel()->item(row,2)->text();
    QString fileName = m_database->getModel()->item(row,3)->text();

    emit levelSelected(name, playCount, rating, fileName);
}

void LevelEditor::makeDrag(QModelIndex index)
{
    QDrag *drag = new QDrag(this);
    QMimeData *data = new QMimeData();
    QString kama = DEFAULT_BLOCKER_DIR + m_database->getBlockModel()->itemFromIndex(index)->text();
    data->setText(kama);
    drag->setMimeData(data);
    drag->setPixmap(QPixmap(kama));
    drag->start();
}

void LevelEditor::on_ratingChanged(int value)
{
    QString name = m_database->getModel()->itemFromIndex(m_selectedIndex)->text();
    m_database->setRating(name, value);

}
