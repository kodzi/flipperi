#include "database.h"
#include "blockeritem.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QFileInfo>
#include <QSettings>
#include <QDir>

Database::Database(QObject *parent) :
    QObject(parent)
{
    m_model = new QStandardItemModel(this);


    m_blockModel = new QStandardItemModel(this);
    QDir directory(DEFAULT_BLOCKER_DIR);
    directory.setFilter(QDir::Files);
    directory.setNameFilters(QStringList() << "*.png");
    QStringList blockList = directory.entryList();
    int x,y = 0;
    QStandardItem *item;
    foreach (QString block, blockList)
    {
        item = new QStandardItem(QIcon(DEFAULT_BLOCKER_DIR +block),block);
        m_blockModel->appendRow(item);
    }
    //Set headers to the model
    QStringList headerSet = (QStringList() << "Name" << "Play count" << "Rating");
    m_model->setColumnCount(3);
    m_model->setHorizontalHeaderLabels(headerSet);

    QString fileName("config.ini");
    QFileInfo fileInfo(fileName);

    if(fileInfo.exists()){

        QSettings settings(fileName, QSettings::IniFormat);
        int size = settings.beginReadArray("levels");
        QList<QStandardItem*> levelInformation;
        for (int i = 0; i < size; ++i){
            settings.setArrayIndex(i);
            levelInformation.append(new QStandardItem(settings.value("name").toString()));
            levelInformation.append(new QStandardItem(settings.value("play count").toString()));
            levelInformation.append(new QStandardItem(settings.value("rating").toString()));
            levelInformation.append(new QStandardItem(settings.value("filename").toString()));
            m_model->appendRow(levelInformation);
            levelInformation.clear();
        }
        settings.endArray();
    }


    //Look for ./kentat/ folder and create new levels with all files inside
    QDir kentat(DEFAULT_LEVEL_DIR);
    kentat.setFilter(QDir::Files); //get only files
    QStringList confFiles = kentat.entryList(QDir::Files);
    QList<QStandardItem*> levelInformation;
    foreach(QString file, confFiles){
        //Level does not exist in model
        if(m_model->findItems(file, Qt::MatchFixedString, 3).size() == 0){
            //Create new level to model with name filename without .conf, play count == 0, rating == 0,
            // filename same as existing filename
            levelInformation.append(new QStandardItem(file.left(file.indexOf(DEFAULT_FILETYPE))));
            levelInformation.append(new QStandardItem("0"));
            levelInformation.append(new QStandardItem("0"));
            levelInformation.append(new QStandardItem(file));
            m_model->appendRow(levelInformation);
            levelInformation.clear();
        }
        else{


        }
    }

}

Database::~Database()
{

    //Clean up m_blockModel
    if(m_blockModel->rowCount() > 0)
    {
        QStandardItem *ite;
        for( int i = 0; i < m_blockModel->rowCount(); ++i)
        {
            ite = m_blockModel->item(i,0);

            delete ite;
            ite = 0;
            ite = m_blockModel->item(i,1);
            delete ite;
            ite = 0;
        }

        m_blockModel->clear();
        delete m_blockModel;
        m_blockModel = 0;
    }

    //First destroy the file
    QFile::remove("config.ini");
    QSettings settings("config.ini", QSettings::IniFormat);
    settings.beginWriteArray("levels");
    QStandardItem* item;
    for(unsigned int i = 0; i < m_model->rowCount(); ++i){
        settings.setArrayIndex(i);
        item = m_model->item(i,0);
        settings.setValue("name", QVariant(item->text()));

        item = m_model->takeItem(i,1);
        settings.setValue("play count", QVariant(item->text()));

        item = m_model->takeItem(i,2);
        settings.setValue("rating",  QVariant(item->text()));

        item = m_model->takeItem(i,3);
        settings.setValue("filename",  QVariant(item->text()));

    }
    settings.endArray();
}


QStandardItemModel* Database::getModel()
{
    return m_model;
}

QStandardItemModel *Database::getBlockModel()
{
    return m_blockModel;

}

QModelIndex Database::addLevelIndex(const QString &name)
{
    QList<QStandardItem*> levelInformation;
    levelInformation.append(new QStandardItem(name));
    levelInformation.append(new QStandardItem(QString::number(0)));
    levelInformation.append(new QStandardItem(QString::number(0)));
    levelInformation.append(new QStandardItem(name + DEFAULT_FILETYPE));
    m_model->appendRow(levelInformation);
    return m_model->indexFromItem(levelInformation.at(0));
}

void Database::writeLevelToFile(Level *newLevel)
{
    QString fileName(DEFAULT_LEVEL_DIR + newLevel->getName() + DEFAULT_FILETYPE);
    //If there is file alredy so first destroy it
    if(QFile::remove(fileName)){
    }

    //Add information to newly created Level
    int id = 1;
    QSettings settings(DEFAULT_LEVEL_DIR + newLevel->getName() + DEFAULT_FILETYPE, QSettings::IniFormat);

    //Add background image
    settings.beginGroup("tausta");
    settings.beginGroup(QString::number(id));
    settings.setValue("kuva", QVariant(newLevel->getBackgroundImage()));
    settings.endGroup();
    settings.endGroup();
    ++id;

    //Add mask image
    settings.beginGroup("pelialue");
    settings.beginGroup(QString::number(id));
    settings.setValue("kuva", QVariant(newLevel->getMaskImage()));
    settings.endGroup();
    settings.endGroup();
    ++id;

    //Add blockers
    settings.beginGroup("este");
    QList<BlockerItem*> blockerList = newLevel->getBlockers();
    foreach (BlockerItem* blocker, blockerList) {
        settings.beginGroup(QString::number(id));
        //Add blocker path and name
        settings.setValue("kuva", QVariant(blocker->getFileName()));
        QPointF pos = blocker->getPos();
        QStringList position;
        position.append(QString::number(pos.x()));
        position.append(QString::number(pos.y()));
        //Add position
        settings.setValue("paikka", QVariant(position));
        ++id;
        settings.endGroup();
    }
    settings.endGroup();
}

void Database::removeLevel(const QModelIndex& modelIndex)
{
    QFile::remove(DEFAULT_LEVEL_DIR + m_model->item(modelIndex.row(),3)->text());
    if(!m_model->removeRow(modelIndex.row())){
    }
}

void Database::changeLevelAttributes(const QModelIndex &index, const QString &newName, const QString &newRating)
{
    m_model->setItem(index.row(),0, new QStandardItem(newName));
    m_model->setItem(index.row(),2, new QStandardItem(newRating));
}

void Database::readLevelFromFile(Level *level)
{
    if(level == 0 ){
        return;
    }
    QString fileName = DEFAULT_LEVEL_DIR + level->getName() + DEFAULT_FILETYPE;
    if(!QFileInfo(fileName).exists()){
        return;
    }

    //Read one level information
    QSettings settings(fileName, QSettings::IniFormat);
    //Read Background image
    settings.beginGroup("tausta");
    QStringList childList = settings.childGroups();
    settings.beginGroup(childList.at(0));
    level->setBackgroundImage(settings.value("kuva").toString());
    settings.endGroup();
    settings.endGroup();

    //Read Mask image
    settings.beginGroup("pelialue");
    childList = settings.childGroups();
    settings.beginGroup(childList.at(0));
    level->setMaskImage(settings.value("kuva").toString());
    settings.endGroup();
    settings.endGroup();
    //Read Blockers into array
    settings.beginGroup("este");
    childList = settings.childGroups();
    BlockerItem* addedBlocker;
    foreach (QString blocker, childList) {
        settings.beginGroup(blocker);
        //Create new blocker
        QStringList position = settings.value("paikka").toStringList();
        QString path = DEFAULT_BLOCKER_DIR + settings.value("kuva").toString();
        QPixmap pix(path);
        int x = position.at(0).toInt();
        int y = position.at(1).toInt();
        QPointF pos(x,y);
        addedBlocker = new BlockerItem(path,pix,pos);
        level->addBlocker(addedBlocker);
        settings.endGroup();
    }
    settings.endGroup();

}

void Database::increasePlayCount(const QString &levelName)
{
    int row = m_model->findItems(levelName).at(0)->row();
    QStandardItem* item = m_model->takeItem(row, 1);
    int num = item->text().toInt();
    item->setText(QString::number(num));
    m_model->setItem(row,1,item);
}
void Database::setRating(const QString &levelName, const int& rating)
{
    int row = m_model->findItems(levelName).at(0)->row();
    QStandardItem* item = m_model->takeItem(row,2);
    item->setText(QString::number(rating));
    m_model->setItem(row, 2,item);
}
