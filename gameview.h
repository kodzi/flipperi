#ifndef GAMEVIEW_H
#define GAMEVIEW_H

#include <QGraphicsView>
#include "DefaultAttributes.h"
#include "level.h"

class GameView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit GameView(QObject *parent = 0);
    GameView(const QString &maskImage, const QString &backgroundImage, QObject *parent = 0);
    void setMaskImage(const QString &mask);
    void setBackgroundImage(const QString &background);

    virtual void dragEnterEvent(QDragEnterEvent *event);
    virtual void dragMoveEvent(QDragMoveEvent *event);
    virtual void dropEvent(QDropEvent *event);

protected:
    virtual void drawBackground(QPainter * painter, const QRectF & rect);
    virtual void mouseMoveEvent(QMouseEvent * event);
    virtual void paintEvent(QPaintEvent *event);
    virtual void resizeEvent(QResizeEvent *event);

signals:
    void addBlocker(const QString &filePath, QPoint position);

private:
    int m_width;
    int m_height;
    QString m_maskImage;
    QString m_backgroundImage;
    QPoint m_dropPosition;
};

#endif // GAMEVIEW_H
