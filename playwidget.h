#ifndef PLAYWIDGET_H
#define PLAYWIDGET_H

#include <QWidget>
#include <QToolButton>
#include <QTimer>
#include <QKeyEvent>
#include "gameview.h"
#include "DefaultAttributes.h"
#include "database.h"

static const qreal LEFT_HANDLE_START_ANGLE = 200;
static const qreal LEFT_HANDLE_END_ANGLE = 115;
static const qreal RIGHT_HANDLE_START_ANGLE = 340;
static const qreal RIGHT_HANDLE_END_ANGLE = 65;

class PlayWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit PlayWidget(Database* database, QWidget *parent = 0);
    ~PlayWidget();
    void startPlay();
    void startEdit();
    void setBackgroundImage(const QString& filePath);
    void setMaskImage(const QString& filePath);
    void setupLevel(QString name, QString playCount, QString rating, QString fileName);
    void deleteSelected();
protected:
    virtual int heightForWidth(int) const;
    virtual void resizeEvent(QResizeEvent *event);
    virtual void keyPressEvent(QKeyEvent *key);
    virtual void keyReleaseEvent(QKeyEvent *key);
private:
    GameView *m_gameView;
    QGraphicsScene *m_gameScene;
    Level* m_level;
    Database* m_database;
    QToolButton* m_backButton;
    QGraphicsPixmapItem* m_ball, *m_right_handle, *m_left_handle;
    QTimer* m_timer;

    qreal xSpeed;
    qreal ySpeed;
    qreal gravity;
    //Borders of game
    QList<QGraphicsItem*> m_listOfAllBorders;

    void doCollison();
    void addBlockers();

signals:
    void changePage(PAGE);

public slots:
    void saveChangesToLevel(bool value);

private slots:
    void on_backButton_clicked();
    void onTimerTimeout();
    void createBlocker(const QString &filePath, QPoint position);
};

#endif // PLAYWIDGET_H
