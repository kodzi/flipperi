#ifndef LEVELEDITOR_H
#define LEVELEDITOR_H

#include "database.h"
#include "DefaultAttributes.h"
#include "playwidget.h"
#include "ratebar.h"
#include <QDialog>
#include <QListView>
#include <QStandardItemModel>


class LevelEditor : public QDialog
{
    Q_OBJECT

public:
    explicit LevelEditor(Database* database, PlayWidget *playWidget, QWidget *parent = 0);
    ~LevelEditor();


    virtual void dragEnterEvent(QDragEnterEvent *event);
    virtual void dragMoveEvent(QDragMoveEvent *event);
signals:
    void changePage(PAGE);
    void levelSelected(QString, QString, QString, QString);
protected:
    virtual void keyPressEvent(QKeyEvent *);
private slots:
    void onSaveRejected();

    void onNewButtonClicked();
    void onRemoveButtonClicked();
    void onBackButtonClicked();
    void onEditButtonClicked();
    void onBackgroundButtonClicked();
    void onMaskButtonClicked();
    void onPlayButtonClicked();
    void makeDrag(QModelIndex);
    void on_ratingChanged(int);
private:
    Database* m_database;
    bool m_editMode;
    PlayWidget* m_playWidget;
    QModelIndex m_selectedIndex;
    RateBar* m_ratebar;
    QListView* m_levelListView, *m_blockerListView;
    QToolButton* m_editButton, *m_newButton, *m_removeButton, *m_maskButton, *m_backgroundButton;
    QPushButton* m_backButton, *m_playButton;
    //This activates or deactivates certain buttons and properties depending on mode parameter
    void setEditMode(bool mode);

};

#endif // LEVELEDITOR_H
