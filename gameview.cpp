#include <QMouseEvent>
#include <QDebug>
#include <QtGui>
#include "gameview.h"

GameView::GameView(QObject *parent): QGraphicsView()
{
    setInteractive(true);
    m_maskImage = DEFAULT_MASK_IMAGE;
    m_backgroundImage = DEFAULT_BACKGROUND_IMAGE;
}

GameView::GameView(const QString &maskImage, const QString &backgroundImage, QObject *parent): QGraphicsView()
{
    setInteractive(true);
    m_maskImage = maskImage;
    m_backgroundImage = backgroundImage;
}

void GameView::setMaskImage(const QString &mask)
{
    if(QFileInfo(mask).exists()){
        m_maskImage = mask;
    }
}

void GameView::setBackgroundImage(const QString &background)
{
    if(QFileInfo(background).exists()){
        m_backgroundImage = background;
    }
}


void GameView::drawBackground(QPainter *painter, const QRectF &rect){
    QImage result = QImage(500,500,QImage::Format_ARGB32_Premultiplied);
    QImage maskImage(m_maskImage);
    QImage backgroundImage(m_backgroundImage);
    painter->drawImage(0,0,backgroundImage);
    painter->setCompositionMode(QPainter::CompositionMode_DestinationAtop);
    painter->drawImage(0,0,maskImage);
    painter->setCompositionMode(QPainter::CompositionMode_DestinationOver);
    painter->fillRect(result.rect(),Qt::white);
    QGraphicsView::drawBackground(painter,rect);
}


void GameView::mouseMoveEvent(QMouseEvent *event)
{

    m_dropPosition = event->pos();
}

void GameView::paintEvent(QPaintEvent *event)
{
    //qDebug() << "painted";
    QGraphicsView::paintEvent(event);
}

void GameView::resizeEvent(QResizeEvent *event)
{
    qDebug() << "resized";
    QGraphicsView::resizeEvent(event);

}
void GameView::dragMoveEvent(QDragMoveEvent *event)
{
    event->accept();
}


void GameView::dropEvent(QDropEvent *event)
{
    emit addBlocker(event->mimeData()->text(), event->pos());
}


void GameView::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}
