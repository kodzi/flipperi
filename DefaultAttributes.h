#ifndef DEFAULTATTRIBUTES_H
#define DEFAULTATTRIBUTES_H

#include <QString>

static const QString DEFAULT_LEVEL_DIR = "kentat/";
static const QString DEFAULT_BACKGROUND_DIR = "tausta/";
static const QString DEFAULT_MASK_DIR = "pelialue/";
static const QString DEFAULT_FILETYPE = ".conf";
static const QString DEFAULT_MASK_IMAGE = "://pelialue/munpelialue.png";
static const QString DEFAULT_BACKGROUND_IMAGE = "://tausta/muntaustakuva.png";
static const QString DEFAULT_BLOCKER_DIR = "este/";
typedef enum{
    PLAY = 0,
    EDIT = 1,
    EXIT = 2,
    BACK = -1
}PAGE;

#endif // DEFAULTATTRIBUTES_H
