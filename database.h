#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QStandardItemModel>
#include "level.h"
#include "DefaultAttributes.h"


/*  This class is used to write and read levels.
 *  All levels are inside folder kentat and they are named as follows:
 *  NAME.conf where NAME == name of the level
 */
class Database : public QObject
{
    Q_OBJECT
public:
    explicit Database(QObject *parent = 0);

    ~Database();

    QStandardItemModel* getModel();
    QStandardItemModel* getBlockModel();

    QModelIndex addLevelIndex(const QString &name);
    /*  Function creates new file for this level and adds its information to it.
     *  If there already is level file with the same name nothing is done.
     *  Parameters:
     *      newLevel is pointer to Level
     *  Conclusion:
     *      File has been created and Level object has been destroyed
     */
    void writeLevelToFile(Level* newLevel);

    /*  Function removes item from m_levelModel corresponding to its item position given as parameter.
     *  It also removes level file that corresponds to given level in the model.
     *  Parameters:
     *      modelIndex is Integer that is used to determine item in a model
     *  Conclusion:
     *      Level file is removed and also its item from model
     */
    void removeLevel(const QModelIndex &modelIndex);

    /*  Function finds level from existing model and changes its values regardless wether they are really
     *  changed.
     *  Parameters:
     *      key is name of the level that is wanted to be changed
     *      newName is the new name for the level
     *      newRating is the new value for the rating
     */
    void changeLevelAttributes(const QModelIndex &index, const QString &newName, const QString &newRating);

    /*  Function reads infromation from file and adds it to level excluding name, play count, rating.
     *  Parameters:
     *      level is Level pointer to exsisting level
     */
    void readLevelFromFile(Level* level);

    void increasePlayCount(const QString& levelName);
    void setRating(const QString& levelName, const int &rating);

signals:

public slots:

private:
    QStandardItemModel *m_model;
    QStandardItemModel *m_blockModel;


};

#endif // DATABASE_H
