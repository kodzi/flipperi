#include "playwidget.h"
#include <QVBoxLayout>
#include <QDebug>
#include <QMessageBox>
#include <QMimeData>
#include <QDrag>


PlayWidget::PlayWidget(Database *database, QWidget *parent) :
    QWidget(parent), m_database(database)
{
    QSizePolicy sizepol(QSizePolicy::Preferred, QSizePolicy::Preferred);
    sizepol.setHeightForWidth(true);
    setSizePolicy(sizepol);

    setMinimumSize(500,500);

    //Parameters for game
    xSpeed = 0;
    ySpeed = -23;
    gravity = 0.5;

    m_level = 0;
    //Create New view
    m_gameView = new GameView(this);
    //Create new Layout
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(m_gameView);
    //Create new Scene
    m_gameScene = new QGraphicsScene(0,0, 500,500,this);
    //Add Scene to view
    m_gameView->setScene(m_gameScene);


    //0 Flipper left side
    m_gameScene->addLine(15,42,15,410,Qt::NoPen);
    //1 Flipper top side
    m_gameScene->addLine(35,6,350,6,Qt::NoPen);
    //2 Spring right side
    m_gameScene->addLine(439, 43, 439, 439, Qt::NoPen);
    //3 Spring bottom
    m_gameScene->addLine(411, 439, 439, 439,Qt::NoPen);
    //4 Spring left side
    m_gameScene->addLine(411, 123, 411, 439,Qt::NoPen);
    //5 Flipper right side
    m_gameScene->addLine(349,123,349, 411,Qt::NoPen);
    //6 Flipper bottom right drop
    m_gameScene->addLine(245,475, 245, 500,Qt::NoPen);
    //7 Flipper bottom left drop
    m_gameScene->addLine(120, 475, 120, 500,Qt::NoPen);
    //8 Flipper bottom ball gap
    m_gameScene->addLine(120, 500, 245, 500,Qt::NoPen);
    //9 Flipper bottom right slope
    QPainterPath bottomRightSlope(QPointF(349,411));
    bottomRightSlope.cubicTo(340,423,327,455,245,475);
    m_gameScene->addPath(bottomRightSlope,Qt::NoPen);

    //10 Flipper bottom left slope
    QPainterPath bottomLeftSlope(QPointF(15,410));
    bottomLeftSlope.cubicTo(32,430,50,456,120,475);
    m_gameScene->addPath(bottomLeftSlope,Qt::NoPen);

    //11 Flipper left top corner
    QPainterPath topLeftCorner(QPointF(15,42));
    topLeftCorner.cubicTo(19,27,20,12,35,6);
    m_gameScene->addPath(topLeftCorner,Qt::NoPen);

    //12 Flipper top right corner
    QPainterPath topRightCorner(QPointF(350,6));
    topRightCorner.cubicTo(375,13,412,10,439,43);
    m_gameScene->addPath(topRightCorner,Qt::NoPen);

    //13 Sping and flipper divider top
    QPainterPath dividerPath(QPointF(349,123));
    dividerPath.cubicTo(350,123,380,77,411,123);
    m_gameScene->addPath(dividerPath,Qt::NoPen);

    //14 Flipper Handles
    m_left_handle = new QGraphicsPixmapItem(QPixmap("://ui/ui_swing_left.png"));
    m_left_handle->setShapeMode(QGraphicsPixmapItem::MaskShape);
    m_left_handle->setScale(-1);
    m_left_handle->setTransformOriginPoint(11,9);
    m_left_handle->setPos(100,470);
    m_left_handle->setRotation(LEFT_HANDLE_START_ANGLE);
    m_gameScene->addItem(m_left_handle);

    //15
    m_right_handle = new QGraphicsPixmapItem(QPixmap("://ui/ui_swing_right.png"));
    m_right_handle->setTransformOriginPoint(59,9);
    m_right_handle->setShapeMode(QGraphicsPixmapItem::MaskShape);
    m_right_handle->setPos(195,470);
    m_right_handle->setRotation(RIGHT_HANDLE_START_ANGLE);
    m_gameScene->addItem(m_right_handle);


    //Button to go back to welcome page
    m_backButton = new QToolButton();
    m_backButton->setGeometry(439,439,62,62);
    m_backButton->setVisible(false);
    m_backButton->setStyleSheet("QToolButton{background-color: transparent;} QToolButton{border-image: url(://ui/ui_back.png);}");
    m_gameScene->addWidget(m_backButton);

    //Ball
    QPixmap pix(20,20);
    pix.load("://ui/g_ball.png");
    m_ball = new QGraphicsPixmapItem(pix);
    m_ball->setPos(415,415);
    m_ball->setShapeMode(QGraphicsPixmapItem::MaskShape);
    m_gameScene->addItem(m_ball);

    //Get all borders from QGraphicsScene
    m_listOfAllBorders =  m_gameScene->items(Qt::AscendingOrder);
    m_listOfAllBorders.removeLast();
    m_listOfAllBorders.removeLast();

    //Add Layout to this widget
    setLayout(mainLayout);

    //Timer
    m_timer = new QTimer(parent);
    m_timer->setInterval(100);
    m_timer->setSingleShot(false);

    connect(m_timer, SIGNAL(timeout()), this, SLOT(onTimerTimeout()));
    connect(m_backButton, SIGNAL(clicked()), this, SLOT(on_backButton_clicked()));
    connect(this, SIGNAL(changePage(PAGE)), parent, SLOT(onChangePage(PAGE)));
    connect(m_gameView, SIGNAL(addBlocker(QString,QPoint)), this, SLOT(createBlocker(QString,QPoint)));
}

PlayWidget::~PlayWidget()
{
    if(m_level != 0){
        delete m_level;
        m_level = 0;
    }
    m_timer->stop();
}



void PlayWidget::startPlay()
{
    grabKeyboard();
    m_backButton->setVisible(true);
    m_gameView->setAcceptDrops(false);

}

void PlayWidget::startEdit()
{
    //Setup editmode
    m_gameView->setAcceptDrops(true);
}


void PlayWidget::setBackgroundImage(const QString &filePath)
{
    m_level->setBackgroundImage(filePath);
    m_gameView->setBackgroundImage(filePath);
}

void PlayWidget::setMaskImage(const QString &filePath)
{
    m_level->setMaskImage(filePath);
    m_gameView->setMaskImage(filePath);
}

void PlayWidget::setupLevel(QString name, QString playCount, QString rating, QString fileName)
{
    qDebug() << "PlayWidget::setupLevel() ";
    m_level = new Level(name,playCount.toInt(),rating.toInt());
    m_database->readLevelFromFile(m_level);
    m_gameView->setBackgroundImage(m_level->getBackgroundImage());
    m_gameView->setMaskImage(m_level->getMaskImage());
    addBlockers();
    m_gameScene->update(0,0,m_gameScene->width(), m_gameScene->height());
}

void PlayWidget::deleteSelected()
{
    QList<QGraphicsItem*> selected = m_gameScene->selectedItems();
    foreach (QGraphicsItem* item, selected) {
        m_gameScene->removeItem(item);
        m_level->removeBlocker(dynamic_cast<BlockerItem*>(item));
        delete item;
        item = 0;
    }
}

int PlayWidget::heightForWidth(int width) const
{
    qDebug() << width;
    return width;
}



void PlayWidget::on_backButton_clicked()
{
    //Access database and increase play count
    m_database->increasePlayCount(m_level->getName());
    if(m_level != 0){
        delete m_level;
        m_level = 0;
    }
    //Shutdown timer
    m_backButton->setVisible(false);
    m_timer->stop();
    //Move ball to start point
    m_ball->setPos(415,415);
    xSpeed = 0;
    ySpeed = -23;
    releaseKeyboard();
    emit changePage(BACK);
}

void PlayWidget::onTimerTimeout()
{
    //Move ball in x and y dimensions
    m_ball->moveBy(xSpeed, ySpeed);
    //Add the efect of gravity
    ySpeed += gravity;
    doCollison();

}

void PlayWidget::createBlocker(const QString &filePath, QPoint position)
{
    //Map to Scene coordinate system
    QPointF pos = m_gameView->mapToScene(position);
    pos.setX((int)pos.x());
    pos.setY((int)pos.y());
    BlockerItem* blocker = new BlockerItem(filePath, QPixmap(filePath), pos);

    if(m_gameScene->collidingItems(blocker).size() != 0){
        delete blocker;
        blocker = 0;
        return;
    }

    m_level->addBlocker(blocker);
    m_gameScene->addItem(blocker);

    m_gameScene->update(0,0,m_gameScene->width(), m_gameScene->height());

}


void PlayWidget::resizeEvent(QResizeEvent *event)
{
    m_gameView->fitInView(m_gameScene->sceneRect(), Qt::KeepAspectRatio);
}

void PlayWidget::keyPressEvent(QKeyEvent *key)
{
    if(key->key() == Qt::Key_Left){
        m_left_handle->setRotation(LEFT_HANDLE_END_ANGLE);
    }
    else if(key->key() == Qt::Key_Right){
        m_right_handle->setRotation(RIGHT_HANDLE_END_ANGLE);
    }
    else if(key->key() == Qt::Key_Down && !m_timer->isActive() && !key->isAutoRepeat()) {
        m_ball->setPos(415,420);
    }

}

void PlayWidget::keyReleaseEvent(QKeyEvent *key)
{
    if(key->key() == Qt::Key_Left){
        m_left_handle->setRotation(LEFT_HANDLE_START_ANGLE);
    }
    else if(key->key() == Qt::Key_Right){
        m_right_handle->setRotation(RIGHT_HANDLE_START_ANGLE);
    }
    else if(key->key() == Qt::Key_Down && !m_timer->isActive() && !key->isAutoRepeat()) {
        //Setup timer for game update
        m_timer->start();
    }
}

void PlayWidget::doCollison()
{
    QList<QGraphicsItem*> collides = m_ball->collidingItems();
    if(collides.size() != 0){
        //DO collision detection
        //Now we only have borders
        foreach (QGraphicsItem* gItem, collides) {
            //Check collison with horizontal borders
            if(gItem == m_listOfAllBorders.at(1) ||
                    gItem == m_listOfAllBorders.at(3)){
                ySpeed *= -0.8;
                if(m_ball->pos().y() < 20){
                    m_ball->moveBy(0,10);
                }
            }
            //Check collison with vertical borders
            else if(gItem == m_listOfAllBorders.at(0) ||
                    gItem == m_listOfAllBorders.at(2) ||
                    gItem == m_listOfAllBorders.at(4) ||
                    gItem == m_listOfAllBorders.at(5) ||
                    gItem == m_listOfAllBorders.at(6) ||
                    gItem == m_listOfAllBorders.at(7)){
                xSpeed *= -0.9;
            }

            //Check if ball goes through ball gap
            else if(gItem == m_listOfAllBorders.at(8)){
                m_timer->stop();
                releaseKeyboard();
                QMessageBox::about(this,"Flipper", "Game Over!");
            }

            //Check collison with flipper bottom right slope
            else if(gItem == m_listOfAllBorders.at(9)){
                ySpeed *= -1;
                xSpeed -= 2;
                //Comes from right
                if(xSpeed <= 0){
                    xSpeed *= 1.1;
                }
                //Comes from left
                else{
                    xSpeed *= 0.8;
                }
                if(m_ball->pos().y() >= 450){
                    m_ball->moveBy(0,-15);
                }
            }
            //Check collison with flipper bottom left slope
            else if(gItem == m_listOfAllBorders.at(10)){
                ySpeed *= -1;
                xSpeed += 2;
                //Comes from right
                if(xSpeed <= 0){
                    xSpeed *= 0.8;
                }
                //Comes from left
                else{
                    xSpeed *= 1.1;
                }
                if(m_ball->pos().y() >= 450){
                    m_ball->moveBy(0,-15);
                }
            }

            //Check collison with flipper left top corner slope
            else if(gItem == m_listOfAllBorders.at(11)){
                ySpeed *= -1;
                //Comes from right
                if(xSpeed <= 0){
                    xSpeed *= 0.8;
                }
                //Comes from left
                else{
                    xSpeed *= 1.1;
                }
            }
            //Check collison with top right corner slope
            else if(gItem == m_listOfAllBorders.at(12)){
                ySpeed = ySpeed * -0.8;
                xSpeed = -1* abs(xSpeed + ySpeed/2);
            }
            //Check collison with Sping and flipper divider top
            //(349,123) -> (411 ,123)
            //center x-coordinate is 380
            else if(gItem == m_listOfAllBorders.at(13)){
                ySpeed *= -0.8;
                //If on the left side of slope
                if(m_ball->x() <= 380){
                    //Comes from the right
                    if(xSpeed <= 0){
                        xSpeed *= 1.2;
                    }
                    //Comes from the left
                    else{
                        xSpeed *= 0.7;
                    }
                }
                //Right side of slope
                else{
                    //Comes from the right
                    if(xSpeed <= 0){
                        xSpeed *= 1.2;
                    }
                    //Comes from the left
                    else{
                        xSpeed *= 0.7;
                    }
                }
            }
            //Check collison with left handle
            else if(gItem == m_listOfAllBorders.at(14)){
                //Left handle is up
                if(m_left_handle->rotation() == LEFT_HANDLE_END_ANGLE){
                    //Comes from right
                    if(xSpeed <= 0){
                        xSpeed *= 1.1;
                        ySpeed *= 0.9;
                    }
                    //Comes from left
                    else{
                        xSpeed *= 0.7;
                    }
                }
                //Left handle is down
                else{
                    ySpeed *= -0.9;
                    //Comes from right
                    if(xSpeed <= 0){
                        xSpeed *= 0.6;
                    }
                    //Comes from left
                    else{
                        xSpeed *= 1.1;
                    }
                    if(abs(ySpeed) < 2){
                        xSpeed += 2;
                    }
                }
            }
            //Check collison with right handle
            else if(gItem == m_listOfAllBorders.at(15)){
                //Right handle is up
                if(m_right_handle->rotation() == RIGHT_HANDLE_END_ANGLE){
                    //Comes from right
                    if(xSpeed <= 0){
                        xSpeed *= 0.7;

                    }
                    //Comes from left
                    else{
                        xSpeed *= 1.1;
                        ySpeed *= 0.9;
                    }
                }
                //Right handle is down
                else{

                    ySpeed *= -0.9;
                    //Comes from right
                    if(xSpeed <= 0){
                        xSpeed *= 1.1;
                    }
                    //Comes from left
                    else{
                        xSpeed *= 0.6;
                    }
                    if(abs(ySpeed) < 2){
                        xSpeed -= 2;
                    }
                }

            }
        }
    }

}

void PlayWidget::addBlockers()
{
    //Get blockers
    foreach (BlockerItem* blocker, m_level->getBlockers()) {
        blocker->setShapeMode(QGraphicsPixmapItem::MaskShape);
        m_gameScene->addItem(blocker);
    }
}


void PlayWidget::saveChangesToLevel(bool value)
{
    if(m_level == 0)
    {
        return;
    }

    //Discard changes made to current level
    foreach(BlockerItem* blocker, m_level->getBlockers())
    {
        m_gameScene->removeItem(blocker);
    }

    if(value)
    {
       m_database->writeLevelToFile(m_level);
    }

    delete m_level;

    m_level = 0;
}

