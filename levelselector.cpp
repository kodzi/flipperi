#include "levelselector.h"
#include "ui_levelselector.h"

#include <QLabel>
#include <QDebug>

LevelSelector::LevelSelector(QStandardItemModel * model,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LevelSelector)
{
    ui->setupUi(this);
    setMinimumSize(500,500);
    setMaximumSize(500,500);
    setWindowTitle("Level Selector");

    //Setup main layout, and layout for filter options
    mainLayout = new QGridLayout(this);
    filterLayout = new QGridLayout(this);

    //Table with sorted model and player can choose only one at a time
    levelChooser = new QTableView(this);
    levelChooser->setSelectionMode(QAbstractItemView::SingleSelection);
    levelChooser->setSelectionBehavior(QAbstractItemView::SelectRows);
    levelChooser->setColumnWidth(0,200);
    //Set so that user cannot change values in the view
    levelChooser->setEditTriggers(QAbstractItemView::NoEditTriggers);

    mainLayout->addWidget(levelChooser,1,0,1,3);



    //Model to show in table
    m_filterModel = new FilterModel(this);
    m_filterModel->setSourceModel(model);
    levelChooser->setModel(m_filterModel);
    levelChooser->setSortingEnabled(true);

    //Hide filepath and id of the level
    levelChooser->hideColumn(3);
    levelChooser->hideColumn(4);

    //Set column width for each column and diasble row numbering
    levelChooser->setColumnWidth(0,265);
    levelChooser->setColumnWidth(1,95);
    levelChooser->setColumnWidth(2,137);
    levelChooser->verticalHeader()->setVisible(false);


    //Filter-inputs container
    filterBox = new QGroupBox("&Filters", this);

    //Name filter
    QLabel* nameFilterLabel = new QLabel("Name:", this);
    filterLayout->addWidget(nameFilterLabel, 0,0);
    nameBox = new QLineEdit();
    filterLayout->addWidget(nameBox,0,1);
    connect(nameBox, SIGNAL(textEdited(QString)), this, SLOT(onNameFilterTextEdited(QString)));

    //PlayCount filter
    QLabel* playCountLabel = new QLabel("Play Count:", this);
    filterLayout->addWidget(playCountLabel,1,0);
    playCountSpinBox = new QSpinBox(this);
    filterLayout->addWidget(playCountSpinBox, 1,1);
    connect(playCountSpinBox, SIGNAL(valueChanged(int)), this, SLOT(onPlayCountFilterChanged(int)));

    //Rating filter
    QLabel* ratingLabel = new QLabel("Rating:",this);
    filterLayout->addWidget(ratingLabel,0,2,1,1,Qt::AlignHCenter);
    filterLayout->setColumnMinimumWidth(2,130);
    filterLayout->setRowMinimumHeight(1,32);
    rateBar = new RateBar(this);
    filterLayout->addWidget(rateBar,1,2);
    rateBar->adjustSize();
    connect(rateBar, SIGNAL(ratingChanged(int)), this, SLOT(onRatingFilterChanged(int)));


    //Buttons to activate selected level or going back to Welcome page
    okButton = new QPushButton("&OK", this);
    connect(okButton, SIGNAL(clicked()), this, SLOT(onOkButtonClicked()));
    mainLayout->addWidget(okButton, 2,2);

    backButton = new QPushButton("&Back", this);
    connect(backButton, SIGNAL(clicked()), this, SLOT(onBackButtonClicked()));
    mainLayout->addWidget(backButton, 3,2);
    filterBox->setLayout(filterLayout);

    mainLayout->addWidget(filterBox,2,0,2,2);
    mainLayout->setGeometry(QRect(0,0,width(),height()));
    mainLayout->setMargin(0);
    mainLayout->setSpacing(3);
    this->setLayout(mainLayout);
}

LevelSelector::~LevelSelector()
{
    delete ui;
}

//Go back to Welcome page
void LevelSelector::onBackButtonClicked()
{
    close();
}

//Emit a signal with selected level-id
void LevelSelector::onOkButtonClicked()
{
    int index = levelChooser->currentIndex().row() ;
    QString name = m_filterModel->data(m_filterModel->index(index,0)).toString();
    QString playCount =m_filterModel->data(m_filterModel->index(index,1)).toString();
    QString rating = m_filterModel->data(m_filterModel->index(index,2)).toString();
    QString fileName = m_filterModel->data(m_filterModel->index(index,3)).toString();
    emit levelSelected(name, playCount, rating, fileName);
    close();
}
//Activate name filter on change
void LevelSelector::onNameFilterTextEdited(QString str)
{
    m_filterModel->setName(str);
}

//Activate rating filter on change
void LevelSelector::onRatingFilterChanged(int score)
{
    m_filterModel->setRating(score);
}

//Activate play count filter on change
void LevelSelector::onPlayCountFilterChanged(int playcount)
{
    m_filterModel->setPlayCount(playcount);
}
