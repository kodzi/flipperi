#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCoreApplication::setOrganizationName("Grako2013");
    QCoreApplication::setOrganizationDomain("parastasoftaa.fi");
    QCoreApplication::setApplicationName("Flipperi");
    MainWindow w;
    w.show();
    
    return a.exec();
}
