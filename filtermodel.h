#ifndef FILTERMODEL_H
#define FILTERMODEL_H

#include <QSortFilterProxyModel>

class FilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit FilterModel(QObject *parent = 0);
    void setRating(int const &rating);
    void setPlayCount(int const &playCount);
    void setName(QString const &name);
signals:

public slots:

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
private:
    int m_playCount;
    int m_rating;
    QString m_name;

    int static const MAX_RATING = 5;

};

#endif // FILTERMODEL_H
