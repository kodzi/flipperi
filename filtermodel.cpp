#include "filtermodel.h"

FilterModel::FilterModel(QObject *parent) :
    QSortFilterProxyModel(parent), m_rating(0), m_playCount(0), m_name("")
{
}

void FilterModel::setRating(const int &rating)
{
    m_rating = rating;
    invalidateFilter();
}

void FilterModel::setPlayCount(const int &playCount)
{
    m_playCount = playCount;
    invalidateFilter();
}

void FilterModel::setName(const QString &name)
{
    m_name = name;
    invalidateFilter();
}

bool FilterModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex nameIndex = sourceModel()->index(source_row,0,source_parent);
    QModelIndex playCountIndex = sourceModel()->index(source_row,1,source_parent);
    QModelIndex ratingIndex = sourceModel()->index(source_row,2,source_parent);
    return (sourceModel()->data(nameIndex).toString().contains(m_name) &&
            sourceModel()->data(playCountIndex).toInt() >= m_playCount &&
            sourceModel()->data(ratingIndex).toInt() >= m_rating);
}

