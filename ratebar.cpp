
#include "ratebar.h"
#include "ui_ratebar.h"
#include <QPushButton>

RateBar::RateBar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RateBar)
{
    m_rating = 0;

    signalMapper = new QSignalMapper(this);


    ui->setupUi(this);

    ui->star1->setStyleSheet("border-image:url(://ui/t_star.png)");
    ui->star2->setStyleSheet("border-image:url(://ui/t_star.png)");
    ui->star3->setStyleSheet("border-image:url(://ui/t_star.png)");
    ui->star4->setStyleSheet("border-image:url(://ui/t_star.png)");
    ui->star5->setStyleSheet("border-image:url(://ui/t_star.png)");

    connect(ui->star1, SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(ui->star1,int(1));
    connect(ui->star2, SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(ui->star2,int(2));
    connect(ui->star3, SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(ui->star3,int(3));
    connect(ui->star4, SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(ui->star4,int(4));
    connect(ui->star5, SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(ui->star5,int(5));


    connect(signalMapper,SIGNAL(mapped(int)),this,SLOT(setRating(int)));
}

RateBar::~RateBar()
{
    delete ui;
}


int RateBar::getRating() const
{
    return m_rating;
}

//Size of the widget
QSize RateBar::sizeHint() const
{
    return QSize(100,32);
}

void RateBar::setRating(int value)
{
    QString filled_star = "border-image: url(://ui/t_star_filled.png);";
    if(value == m_rating)
    {
        value -= 1;
    }
    m_rating = value;

    setStars(value,filled_star);
    emit ratingChanged(m_rating);
}

void RateBar::setStars(const int &value, QString &filled_star)
{
    QString blank_star = "border-image: url(://ui/t_star.png);";

    if( value >= 1)
    {
        ui->star1->setStyleSheet(filled_star);
    }else
    {
        ui->star1->setStyleSheet(blank_star);
    }
    if(value >= 2)
    {
        ui->star2->setStyleSheet(filled_star);
    }else
    {
        ui->star2->setStyleSheet(blank_star);
    }
    if(value >= 3)
    {
        ui->star3->setStyleSheet(filled_star);
    }else
    {
        ui->star3->setStyleSheet(blank_star);
    }
    if(value >= 4)
    {
        ui->star4->setStyleSheet(filled_star);
    }else
    {
        ui->star4->setStyleSheet(blank_star);
    }
    if(value >= 5)
    {
        ui->star5->setStyleSheet(filled_star);
    }
    else
    {
        ui->star5->setStyleSheet(blank_star);
    }
}

