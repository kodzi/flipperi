#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "welcomewidget.h"
#include "aboutdialog.h"
#include "playwidget.h"
#include "blockeritem.h"
#include <QVBoxLayout>
#include <QDebug>
#include <QResizeEvent>
#include <QFileInfo>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    alreadyResized = false;


    //Model
    //0: name
    //1: play count
    //2: rating
    //3: filepath


    m_database = new Database(this);

    //Setup pages
    stackedPages = new QStackedWidget(this);

    //Welcome aka First page
    welcomePage = new WelcomeWidget(this);
    stackedPages->addWidget(welcomePage);

    //View for gameplay and level editing
    playPage = new PlayWidget(m_database,this);
    stackedPages->addWidget(playPage);

    //Level Editor
    levelEditorDialog = new LevelEditor(m_database, playPage, this);
    connect(levelEditorDialog, SIGNAL(changePage(PAGE)), this, SLOT(onChangePage(PAGE)));
    connect(levelEditorDialog, SIGNAL(levelSelected(QString,QString,QString,QString)), this, SLOT(onLevelSelected(QString,QString,QString,QString)));


    //Level Selector
    levelSelectorDialog = new LevelSelector(m_database->getModel(),this);
    connect(levelSelectorDialog, SIGNAL(levelSelected(QString,QString,QString,QString)), this, SLOT(onLevelSelected(QString,QString,QString,QString)));

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(stackedPages);
    ui->centralWidget->setLayout(layout);
    stackedPages->setCurrentWidget(welcomePage);


    //Set minimum size for window,  Width: 524 and Height 524, margin = 12
    setMinimumSize(524, 524);

    //Get settings
    QSettings settings;
    restoreGeometry(settings.value("mainwindow/geo").toByteArray());
}

MainWindow::~MainWindow()
{
    //Save current geometry settings
    QSettings settings;
    settings.setValue("mainwindow/geo",saveGeometry());
    delete ui;
}

void MainWindow::onChangePage(PAGE act)
{
    switch(act){
    case PLAY:
        stackedPages->setCurrentWidget(playPage);
        playPage->startPlay();
        break;
    case EDIT:
        stackedPages->setCurrentWidget(playPage);
        levelEditorDialog->move(pos().x()+ 20 + width(), pos().y());
        levelEditorDialog->show();
        break;
    case EXIT:
        close();
        break;
    case BACK:
        stackedPages->setCurrentWidget(welcomePage);
    default:
        break;
    }
}

void MainWindow::onLevelSelected(QString name, QString playCount, QString rating, QString fileName)
{
    playPage->setupLevel(name, playCount,rating, fileName);
    onChangePage(PLAY);
}

//Show LevelSelector dialog
void MainWindow::showLevelSelector()
{
    levelSelectorDialog->exec();
    levelSelectorDialog->move(this->geometry().center());
}


void MainWindow::on_action_About_triggered()
{
    AboutDialog about;
    about.exec();
}

void MainWindow::moveEvent(QMoveEvent *event)
{
    if(stackedPages->currentWidget() == playPage){
        //These parameters are used to tweak the interface so the two widget move side by side
        levelEditorDialog->move(pos().x()+ 20 + width(), pos().y());
    }
}
