#include "blockeritem.h"
#include "DefaultAttributes.h"
#include <QString>

#ifndef LEVEL_H
#define LEVEL_H

class Level
{
public:
    Level(const QString &name );
    Level(const QString &name, const int &playCount, const int &rating);
    ~Level();
    //void addBlocker(BlockerItem* blocker);
    void setMaskImage(const QString& filePath);
    void setBackgroundImage(const QString &filePath);
    void setRating(int rating);
    void increasePlayCount();
    QString getName() const;
    int getId() const;
    int getRating() const;
    int getPlayCount() const;
    QString getMaskImage() const;
    QString getBackgroundImage() const;
    QList<BlockerItem *> getBlockers() const;
    void addBlocker(BlockerItem *blocker);
    void removeBlocker(BlockerItem* blocker);
private:
    QString m_name;
    QString m_background;
    QString m_mask;
    int m_playCount;
    QList<BlockerItem *> m_blockers;
    int m_id;
    int m_rating;
};

#endif // LEVEL_H
