#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "database.h"
#include "leveleditor.h"
#include "levelselector.h"

#include <QMainWindow>
#include <QSettings>
#include <QStackedWidget>
#include <QStandardItemModel>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onChangePage(PAGE act);
    void onLevelSelected(QString name, QString playCount, QString rating, QString fileName);
    void showLevelSelector();

private slots:
    void on_action_About_triggered();

protected:
    virtual void moveEvent(QMoveEvent * event);

private:
    Ui::MainWindow *ui;
    QWidget* welcomePage;
    LevelSelector *levelSelectorDialog;
    PlayWidget* playPage;
    LevelEditor *levelEditorDialog;
    QStackedWidget *stackedPages;
    Database* m_database;

    QString selectedLevel; //TEST LevelSelectorDialog
    bool alreadyResized;

};

#endif // MAINWINDOW_H
