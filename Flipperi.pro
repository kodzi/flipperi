#-------------------------------------------------
#
# Project created by QtCreator 2013-10-07T19:09:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Flipperi
TEMPLATE = app
VERSION = 0.5.0

SOURCES += main.cpp\
        mainwindow.cpp \
    welcomewidget.cpp \
    aboutdialog.cpp \
    playwidget.cpp \
    level.cpp \
    ratebar.cpp \
    filtermodel.cpp \
    gameview.cpp \
    levelselector.cpp \
    leveleditor.cpp \
    database.cpp \
    blockeritem.cpp

HEADERS  += mainwindow.h \
    welcomewidget.h \
    aboutdialog.h \
    playwidget.h \
    level.h \
    ratebar.h \
    filtermodel.h \
    gameview.h \
    levelselector.h \
    leveleditor.h \
    database.h \
    blockeritem.h \
    DefaultAttributes.h

FORMS    += mainwindow.ui \
    aboutdialog.ui \
    ratebar.ui \
    levelselector.ui

RESOURCES += \
    resource.qrc
