#include "level.h"
#include <QFile>
#include <QDebug>


//Takes files name as parameter an reads from it all necessary data
Level::Level(const QString &name)
{
    m_name = name;
    m_playCount = 0;
    m_rating = 0;
    m_mask = DEFAULT_MASK_IMAGE;
    m_background = DEFAULT_BACKGROUND_IMAGE;
}

Level::Level(const QString &name, const int &playCount, const int &rating):
    m_name(name), m_playCount(playCount), m_rating(rating)
{
}

//Saves changes
Level::~Level()
{
    if(!m_blockers.isEmpty()){
        foreach (BlockerItem* blocker, m_blockers) {
            delete blocker;
            blocker = 0;
        }
        m_blockers.clear();
    }
}

void Level::setMaskImage(const QString &filePath)
{
    m_mask = filePath;
}

void Level::setBackgroundImage(const QString& filePath)
{
    m_background = filePath;
}


void Level::setRating(int rating)
{
    m_rating = rating;
}

void Level::increasePlayCount()
{
    m_playCount++;
}

QString Level::getName() const
{
    return m_name;
}

int Level::getId() const
{
    return m_id;
}

int Level::getRating() const
{
    return m_rating;
}

int Level::getPlayCount() const
{
    return m_playCount;
}

QString Level::getMaskImage() const
{
    return m_mask;
}

QString Level::getBackgroundImage() const
{
    return m_background;
}



QList<BlockerItem *> Level::getBlockers() const
{
    return m_blockers;
}

void Level::addBlocker(BlockerItem *blocker)
{
    m_blockers.append(blocker);
}

void Level::removeBlocker(BlockerItem *blocker)
{
    m_blockers.removeOne(blocker);
    blocker = 0;
}

