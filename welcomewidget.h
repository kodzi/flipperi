#ifndef WELCOMEWIDGET_H
#define WELCOMEWIDGET_H

#include <QWidget>
#include <QGraphicsTextItem>
#include <QGraphicsScene>
#include <QPushButton>
#include "DefaultAttributes.h"


class WelcomeWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit WelcomeWidget(QWidget *parent = 0);
    ~WelcomeWidget();
    
private:

    QGraphicsScene* scene;

    QPushButton *m_play_btn;
    QPushButton *m_editLevels_btn;
    QPushButton *m_exit_btn;

private slots:
    void onPlayClicked();
    void onEditClicked();
    void onExitClicked();

signals:
    void changePage(PAGE);
};

#endif // WELCOMEWIDGET_H
