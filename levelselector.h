#ifndef LEVELSELECTOR_H
#define LEVELSELECTOR_H

#include "filtermodel.h"
#include "ratebar.h"

#include <QDialog>
#include <QGridLayout>
#include <QGroupBox>
#include <QSpinBox>
#include <QStandardItemModel>
#include <QTableView>
#include <QLineEdit>
#include <QPushButton>

namespace Ui {
class LevelSelector;
}

class LevelSelector : public QDialog
{
    Q_OBJECT

public:
    explicit LevelSelector(QStandardItemModel * model, QWidget *parent = 0);
    ~LevelSelector();

signals:
    void levelSelected(QString, QString, QString, QString);
public slots:
    void onBackButtonClicked();
    void onOkButtonClicked();
    void onNameFilterTextEdited(QString);
    void onRatingFilterChanged(int);
    void onPlayCountFilterChanged(int);

private:
    Ui::LevelSelector *ui;
    FilterModel *m_filterModel;
    QPushButton* okButton;
    QPushButton* backButton;

    QTableView* levelChooser;

    //Filters
    QGroupBox *filterBox;
    QLineEdit *nameBox;
    QSpinBox* playCountSpinBox;
    RateBar *rateBar;

    //Layouts
    QGridLayout *mainLayout;
    QGridLayout *filterLayout;
};

#endif // LEVELSELECTOR_H
