#ifndef RATEBAR_H
#define RATEBAR_H

#include <QWidget>
#include <QSignalMapper>
#include "ui_ratebar.h"

namespace Ui {
class RateBar;
}

class RateBar : public QWidget
{
    Q_OBJECT
    
public:
    explicit RateBar(QWidget *parent = 0);
    ~RateBar();
    
    int getRating() const;
    virtual QSize sizeHint() const;
signals:
    void ratingChanged(int);

public slots:
    //void hoverRating(int value);
    void setRating(int value);

private:
    Ui::RateBar *ui;
    int m_rating;
    QString m_star_img;
    QSignalMapper *signalMapper;

    //Set amount of stars to be filled and fill-image
    void setStars(const int &value,QString &filled_star);
};

#endif // RATEBAR_H
