#include "welcomewidget.h"
#include <QDebug>
#include <QVBoxLayout>
#include <QGraphicsView>

WelcomeWidget::WelcomeWidget(QWidget *parent) :
    QWidget(parent)

{
    QString play_btn_style = "QPushButton {background-color: transparent;} QPushButton {border-image: url(://ui/btn_play.png);} QPushButton:hover { border-image: url(://ui/btn_play_hovered.png);}";
    QString lvls_btn_style = "QPushButton {background-color: transparent;} QPushButton {border-image: url(://ui/btn_edit_levels.png);} QPushButton:hover { border-image: url(://ui/btn_edit_levels_hovered.png);}";
    QString exit_btn_style = "QPushButton {background-color: transparent;} QPushButton {border-image: url(://ui/btn_exit.png);} QPushButton:hover { border-image: url(://ui/btn_exit_hovered.png);}";

    //Create new scene
    scene = new QGraphicsScene(this);
    //Add scene to graphcisView

    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    setLayout(mainLayout);
    QGraphicsView* graphicsView = new QGraphicsView(this);
    mainLayout->addWidget(graphicsView);
    graphicsView->setScene(scene);

    //background
    QBrush backgroundBrush(QImage("://ui/bg_tausta01.png"));
    scene->setBackgroundBrush(backgroundBrush);
    connect(this, SIGNAL(changePage(PAGE)), parent, SLOT(onChangePage(PAGE)));

    m_play_btn = new QPushButton();
    m_editLevels_btn = new QPushButton();
    m_exit_btn = new QPushButton();

    //Play button
    m_play_btn->setGeometry(0,0,256,84);
    m_play_btn->setFlat(true);
    m_play_btn->setStyleSheet(play_btn_style);

    //Edit levels button
    m_editLevels_btn->setGeometry(0,84,256,84);
    m_editLevels_btn->setStyleSheet(lvls_btn_style);

    //Exit button
    m_exit_btn->setGeometry(0,230,256,84);
    m_exit_btn->setStyleSheet(exit_btn_style);

    //Add to scene
    scene->addWidget(m_play_btn);
    scene->addWidget(m_editLevels_btn);
    scene->addWidget(m_exit_btn);

    connect(m_play_btn, SIGNAL(clicked()), parent, SLOT(showLevelSelector()));
    connect(m_editLevels_btn, SIGNAL(clicked()), this, SLOT(onEditClicked()));
    connect(m_exit_btn, SIGNAL(clicked()), this, SLOT(onExitClicked()));

}

WelcomeWidget::~WelcomeWidget()
{
}


void WelcomeWidget::onPlayClicked()
{
    qDebug() << "PlayButton painettu";
    emit changePage(PLAY);
}

void WelcomeWidget::onEditClicked()
{
    qDebug() << "EditButton painettu";
    emit changePage(EDIT);
}

void WelcomeWidget::onExitClicked()
{
    qDebug() << "ExitButton painettu";
    emit changePage(EXIT);
}




