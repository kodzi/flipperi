#ifndef BLOCKERITEM_H
#define BLOCKERITEM_H

#include <QGraphicsPixmapItem>

class BlockerItem : public QGraphicsPixmapItem
{
public:
    BlockerItem(QGraphicsItem *parent = 0);
    BlockerItem(const QString& fileName, const QPixmap& pixmap, const QPointF& pos, QGraphicsItem * parent = 0 );
    QString getFileName() const;
    QPointF getPos() const;

private:
    /*  Determine how much kinetic energy does object absorb when collided with ball
     *  m_bounce < 1
     */
    double m_bounce;
    // Tells how many points player gets when collide with ball
    int m_score;
    // Stores filName with path
    QString m_fileName;
    QPointF m_pos;

};

#endif // BLOCKERITEM_H
